import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] primeNumber = new int[5];

        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;

        System.out.println("The First prime number is: " + primeNumber[0]);
        System.out.println("The Second prime number is: " + primeNumber[1]);
        System.out.println("The Third prime number is: " + primeNumber[2]);
        System.out.println("The Fourth prime number is: " + primeNumber[3]);
        System.out.println("The Fifth prime number is: " + primeNumber[4]);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("Toothpaste", 15);
                put("Toothbrush", 20);
                put("Soap", 12);
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);
    }
}